/*
 *  Copyright, 2023-2024 Haiku, Inc. All rights reserved.
 *  Released under the terms of the MIT license.
 *
 *  Authors:
 *  	Alexander von Gluck <kallisti5@unixzen.com>
 */

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef __HAIKU__
#include <kernel/fs_volume.h>
#endif

int verbose = 0;


int
usage(int ret)
{
	fprintf(stderr, "mkroot [-h] [-v] path\n");
	return ret;
}


int
prep_packages(char* path)
{
	char target[PATH_MAX];
	snprintf(target, PATH_MAX, "%s/boot/system/packages/haiku-r1~beta4_hrev57627-1-x86_64.hpkg", path);
	return symlink("/boot/system/packages/haiku-r1~beta4_hrev57627-1-x86_64.hpkg", target);
}


int
prep_sysroot(char* path)
{
	// Create all the required directories for the sysroot
	const char* staticPaths[] = {
		"dev", "boot", "boot/system", "boot/system/packages",
		"boot/system/cache", "boot/system/cache/tmp", "boot/settings",
		"boot/system/var", "boot/system/var/shared_memory",
		NULL
	};

	int i = 0;
	while (staticPaths[i] != NULL) {
		char buffer[PATH_MAX];
		int status = snprintf(buffer, PATH_MAX, "%s/%s", path, staticPaths[i]);
		if (status < 0) {
			printf("Error: unable to form path '%s/%s'\n", path, staticPaths[i]);
			return status;
		}
		if (verbose)
			printf("mkdir('%s')\n", buffer);
		status = mkdir(buffer, S_IRWXU | S_IRWXG | S_IRWXO);
		if (status < 0) {
			printf("Error: Unable to create directory %s: %s\n", buffer, strerror(errno));
			return status;
		}
		i++;
	}

	return 0;
}

int
mount_sysroot(char* path)
{
#ifdef __HAIKU__
	char buffer[PATH_MAX];
	snprintf(buffer, PATH_MAX, "%s/dev", path);

	dev_t volume = fs_mount_volume(buffer, NULL, "bindfs", 0, "source /dev");
	if (volume < B_OK) {
		fprintf(stderr, "Error: %s\n", strerror(volume));
		return 1;
	}

	snprintf(buffer, PATH_MAX, "%s/boot/system", path);
	volume = fs_mount_volume(buffer, NULL, "packagefs", 0, "type system");
	if (volume < B_OK) {
		fprintf(stderr, "Error: %s\n", strerror(volume));
		return 1;
	}
#endif

	return 0;
}


int
viable_sysroot(char* path)
{
	struct stat stats;
	stat(path, &stats);
	if (!S_ISDIR(stats.st_mode)) {
		printf("Error: provided target path is not a directory!\n");
		return 1;
	}

	memset(&stats, 0, sizeof(stats));

	char buffer[PATH_MAX];
	snprintf(buffer, PATH_MAX, "%s/boot", path);
	stat(buffer, &stats);
	if (S_ISDIR(stats.st_mode)) {
		printf("Error: provided target path is dirty and contains remnants!\n");
		return 1;
	}

	return 0;
}


int
main(int argc, char **argv)
{
	if (argc < 2)
		return usage(1);

	int ch;
	while ((ch = getopt(argc, argv, "hv")) != -1) {
		switch (ch) {
		case 'v':
			verbose = 1;
			break;
		case 'h':
			return usage(0);
		default:
			return usage(1);
		}
	}
	if (argv[optind] == NULL)
		return usage(1);

	// Initial sanity checks
	if (viable_sysroot(argv[optind]) != 0)
		return 1;

	prep_sysroot(argv[optind]);
	prep_packages(argv[optind]);
	mount_sysroot(argv[optind]);
}
